﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using Microsoft.Owin.Security.OAuth;
using NasiFestivali.Interfaces;
using NasiFestivali.Repository;
using NasiFestivali.Resolver;
using Newtonsoft.Json.Serialization;
using Unity;
using Unity.Lifetime;

namespace NasiFestivali
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            // Configure Web API to use only bearer token authentication.
            config.SuppressDefaultHostAuthentication();
            config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            // Unity  !moraju se obe klase Registrovati
            var container = new UnityContainer();
            container.RegisterType<IFestivalsRepository, FestivalsRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<IPlacesRepository, PlacesRepository>(new HierarchicalLifetimeManager());
            config.DependencyResolver = new UnityResolver(container);
        }
    }
}
