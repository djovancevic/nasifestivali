﻿using NasiFestivali.Interfaces;
using NasiFestivali.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace NasiFestivali.Controllers
{
    public class FestivaliController : ApiController
    {
        IFestivalsRepository _repository { get; set; }

        public FestivaliController(IFestivalsRepository repository)
        {
            _repository = repository;
        }

        //GET api/festivali

        public IEnumerable<Festival> Get()
        {
            return _repository.GetAll();
        }

        //public IQueryable<FestivalDTO> GetFestivals()  //Zar nece biti zbunjen kad posaljem get da li da bira EFmodel ili DTO???
        //{
        //    Configuration.Services.GetTraceWriter().Info(
        //            Request,
        //            "BooksController",
        //            "Dobavi sve knjige skraceno."
        //        );
        //    return _repository.Get();
        //}

        //GET api/festivali/{id}
        [ResponseType(typeof(Festival))]
        public IHttpActionResult Get(int id)
        {
            var festival = _repository.GetById(id);
            if (festival == null)
            {
                return NotFound();
            }
            return Ok(festival);
        }

        //POST api/festivali
        [Authorize]
        [ResponseType(typeof(Festival))]
        public IHttpActionResult Post(Festival festival)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _repository.Add(festival);
            return CreatedAtRoute("DefaultApi", new { id = festival.Id }, festival);
        }

        //PUT api/festivali/{id}
        [ResponseType(typeof(Festival))]
        public IHttpActionResult Put(int id, Festival festival)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != festival.Id)
            {
                return BadRequest();
            }

            try
            {
                _repository.Update(festival);
            }
            catch
            {
                return BadRequest();
            }

            return Ok(festival);
        }

        //DELETE api/festivali/{id}
        [Authorize]
        [ResponseType(typeof(void))]
        public IHttpActionResult Delete(int id)
        {
            var festival = _repository.GetById(id);
            if (festival == null)
            {
                return NotFound();
            }

            _repository.Delete(festival);
            return Ok();
        }

        // POST api/festivali/pretraga
        [Authorize]
        [Route("api/festivali/pretraga")]
        [HttpPost]
        public IEnumerable<Festival> Pretraga(StartKraj startKraj )
        {
            var start = startKraj.Start;
            var kraj = startKraj.Kraj;
            return _repository.GetFestivalsInFirstHeldYearRange(start, kraj);
        }
    }
}
