﻿using NasiFestivali.Interfaces;
using NasiFestivali.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace NasiFestivali.Controllers
{
    public class MestaController : ApiController
    {
        public IPlacesRepository _repository { get; set; }

        public MestaController(IPlacesRepository repository)
        {
            _repository = repository;
        }

        // GET api/mesta

        public IEnumerable<Place> Get()
        {
            return _repository.GetAll();
        }

        // GET api/mesta/{id}
        [ResponseType(typeof(Place))]
        public IHttpActionResult Get(int id)
        {
            var place = _repository.GetById(id);
            if (place == null)
            {
                return NotFound();
            }
            return Ok(place);
        }

        //GET api/mesta?kod={vrednost}
        //[Route("{kod=vrednost}")]
        public IEnumerable<Place> GetPlacesByZipLessThenGiven(int kod)
        {
            return _repository.GetPlacesByZipLessThenGiven(kod);
        }
    }
}
