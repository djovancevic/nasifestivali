namespace NasiFestivali.Migrations
{
    using NasiFestivali.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<NasiFestivali.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(NasiFestivali.Models.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
            context.Places.AddOrUpdate(new Place() { Id = 1, PlaceName = "Novi Sad", Zip = 21000 });
            context.Places.AddOrUpdate(new Place() { Id = 2, PlaceName = "Szeged", Zip = 9000 });
            context.Places.AddOrUpdate(new Place() { Id = 3, PlaceName = "Berlin", Zip = 1000 });

            context.SaveChanges();



            context.Festivals.AddOrUpdate(new Festival() { Id = 1, FestivalName = "Exit", Price = 50, FirstHeldYear = 2001, PlaceId = 1 });
            context.Festivals.AddOrUpdate(new Festival() { Id = 2, FestivalName = "Siget", Price = 100, FirstHeldYear = 1998, PlaceId = 2 });
            context.Festivals.AddOrUpdate(new Festival() { Id = 3, FestivalName = "Love Parade", Price = 200, FirstHeldYear = 1995, PlaceId = 3 });


            context.SaveChanges();




        }
    }
}
