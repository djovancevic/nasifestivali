﻿using NasiFestivali.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NasiFestivali.Interfaces
{
    public interface IPlacesRepository
    {
        IEnumerable<Place> GetAll();
        Place GetById(int id);
        IQueryable<Place> GetPlacesByZipLessThenGiven(int zip);
    }
}
