﻿using NasiFestivali.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NasiFestivali.Interfaces
{
    public interface IFestivalsRepository
    {
        IQueryable<Festival> GetAll();
        Festival GetById(int id);
        void Add(Festival festival);
        void Update(Festival festival);
        void Delete(Festival festival);
        IEnumerable<Festival> GetFestivalsInFirstHeldYearRange(int start, int kraj);
    }
}
