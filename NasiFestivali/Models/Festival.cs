﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NasiFestivali.Models
{
    public class Festival
    {
        public int Id { get; set; }
        [Required]
        public string FestivalName { get; set; }
        [Required]
        [Range(0.01, double.MaxValue)]
        public double Price { get; set; }
        [Required]
        [Range(1951, 2017)]
        public int FirstHeldYear { get; set; }

        //foreign key
        public int PlaceId { get; set; }
        public Place Place { get; set; }
    }
}