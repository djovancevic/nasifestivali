﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NasiFestivali.Models
{
    public class Place
    {
        public int Id { get; set; }
        [Required]
        public string PlaceName { get; set; }
        [Required]
        [Range(1, 99999)]
        public int Zip { get; set; }
    }
}