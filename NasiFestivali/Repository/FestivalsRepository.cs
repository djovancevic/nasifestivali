﻿using NasiFestivali.Interfaces;
using NasiFestivali.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;

namespace NasiFestivali.Repository
{
    public class FestivalsRepository : IDisposable, IFestivalsRepository
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public IQueryable<Festival> GetAll()
        {
            return db.Festivals.Include(p => p.Place).OrderByDescending(p => p.Price);
        }

       


        public Festival GetById(int id)
        {
            return db.Festivals.FirstOrDefault(p => p.Id == id);
        }

        public void Add(Festival festival)
        {
            db.Festivals.Add(festival);
            db.SaveChanges();
        }

        public void Update(Festival festival)
        {
            db.Entry(festival).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
        }

        public void Delete(Festival festival)
        {
            db.Festivals.Remove(festival);
            db.SaveChanges();
        }



        public IEnumerable<Festival> GetFestivalsInFirstHeldYearRange(int start, int kraj)
        {

            var festivalsInRange = db.Festivals.Include(p => p.Place).Where(p => p.FirstHeldYear > start && p.FirstHeldYear < kraj).OrderBy(p => p.FirstHeldYear);
            return festivalsInRange;
        }






        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}