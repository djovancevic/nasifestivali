﻿using NasiFestivali.Interfaces;
using NasiFestivali.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NasiFestivali.Repository
{
    public class PlacesRepository : IDisposable, IPlacesRepository
    {
        private ApplicationDbContext db = new ApplicationDbContext();


        public IEnumerable<Place> GetAll()
        {
            return db.Places;
        }


        public Place GetById(int id)
        {
            return db.Places.FirstOrDefault(p => p.Id == id);
        }

        public void Add(Place place)
        {
            db.Places.Add(place);
            db.SaveChanges();
        }

        public IQueryable<Place> GetPlacesByZipLessThenGiven(int zip)
        {
            var PlacesByZipLessThenGiven = db.Places.Where(p => p.Zip < zip).OrderBy(p => p.Zip);
            return PlacesByZipLessThenGiven;
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}