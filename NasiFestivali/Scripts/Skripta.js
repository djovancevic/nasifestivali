﻿$(document).ready(function () {
    //podaci od interesa
    var host = window.location.host;
    var token = null;
    var httpHeaders = {};
    var festivaliEndpoint = "/api/festivali/";
    var mestaEndpoint = "/api/mesta/";
    var formAction = "Create";
    var editingId;//koristi se u obradi submita forme - PUT api/products
    // razlikuje se od editId - u funkciji za edit

    //pripremanje dogadjaja za brisanje
    $("body").on("click", "#btnDelete", deleteFestival);


    //pripremanje dogadjaja za izmenu
    $("body").on("click", "#btnEdit", editFestival);


    //posto inicijalno nismo prijavljeni sakrivamo odjavu
    $("#odjava").css("display", "none");

    //triggerovanje tabele  odmah na pocetku
    $("#btnTabela").on("click", ucitajEntiteteVise).trigger("click");



    //prelazak na registraciju
    $("#prelazNaRegistraciju").click(function () {
        $("#prijava").css("display", "none");
        $("#registracija").css("display", "block");

    });


    //registracija korisnika
    $("#registracija").submit(function (e) {
        e.preventDefault();

        var email = $("#regEmail").val();
        var loz1 = $("#regLoz").val();
        var loz2 = $("#regLoz2").val();
        //objekat koji se salje
        var sendData = {
            "Email": email,
            "Password": loz1,
            "ConfirmPassword": loz2
        };

        $.ajax({
            type: "POST",
            url: 'http://' + host + "/api/Account/Register",
            data: sendData
        }).done(function (data) {
            $("#registracija").css("display", "none");
            $("#prijava").css("display", "block");//skloni formu za registraciju, postavi formu za prijavu
        }).fail(function (data) {
            alert("Registracija nije bila uspesna pokusajte ponovo");
        });

    });



    //prijava korisnika
    $("#prijava").submit(function (e) {
        e.preventDefault();

        var email = $("#priEmail").val();
        var loz = $("#priLoz").val();
        var urlRequest = 'http://' + host + "/Token";

        //objekat koji se salje
        var sendData = {
            "grant_type": "password",
            "username": email,
            "password": loz
        };

        $.ajax({
            type: "POST",
            url: urlRequest,
            data: sendData
        }).done(function (data) {
            console.log(data);
            $("#info").empty().css("display", "block").append("Prijavljeni korisnik: " + data.userName);
            //Ovde se uzima Token
            token = data.access_token;
            $("#prijava").css("display", "none");
            //$("#registracija").css("display", "none");
            $("#odjava").css("display", "block");

            //prikazujem funkciju za brisanje/izmenu entiteta
            $("#headerObrisi").css("display", "block");
            $("#headerIzmeni").css("display", "block");
            $("td[style='display:none']").css("display", "block");

            //poziv funkcije za izmenu entiteta
            //$("#headerIzmeni").css("display", "block");
            //$("td[button]").css("display", "block").on('click', function () {
            //editFestival();
            //});
            //prikazujem funkciju za ucitavanje filtera
            $("#divZaPretragu").css("display", "block");
            //dodati u html formu u bootstrep gridu

            //pozivam funkciju za ucitavanje dropboxa
            populateDropbox();
            //pozivam funkciju za ucitavanje forme
            $("#formCreate").css("display", "block");
        }).fail(function (data) {
            alert("Autentikacija nije uspela. Pokusajte ponovo.")
        });


    });

    //odjava korisnika sa sistema
    $("#odjava").click(function () {
        token = null;
        httpHeaders = {};

        $("#prijava").css("display", "block");
        refreshPrijava();
        $("#registracija").css("display", "none");
        $("#odjava").css("display", "none");
        $("#info").empty();
        refreshTable();
        $("#divZaPretragu").css("display", "none");
        $("#formCreate").css("display", "none");

    });


    //Get api/entiteti/1
    //ucitavanje jednog entiteta
    //$("#ucitaj1Entitet").click(function () {
    //    //korisnik mora biti ulogovan
    //    if (token) {
    //        headers.Authorization = 'Bearer' + token;
    //    }

    //    $.ajax({
    //        type: "GET",
    //        url: "http://" + host + "/api/products/1",
    //        headers: httpHeaders

    //    }).done(function (data) {
    //        $("#sadrzaj").append("Proizvod: " + data.Name);

    //    }).fail(function (data) {
    //        alert(data.status + ": " + "Poziv funkcionalnosti nije uspeo.")

    //    });

    //});


    //CRUD



    //GET api/festivali
    //prikaz tabele sa entitetima-vise
    //

    function ucitajEntiteteVise() {

        //ucitavanje entiteta
        var requestUrl = "http://" + host + festivaliEndpoint;
        console.log("URL zahteva: " + requestUrl);
        if (token) {
            httpHeaders.Authorization = 'Bearer ' + token;
        }
        $.ajax({
            type: "GET",
            url: requestUrl,
            headers: httpHeaders

        }).done(ucitajTabelu)//ovde ucitava tabelu entiteti-vise
            .fail(function (data) {
                alert(data.status + ": " + "Poziv funcionalnosti nije uspeo");

            });
    };


    //metoda za postavljanje entiteta-vise u tabelu
    function ucitajTabelu(data, status) {
        console.log("Status: " + status);
        //$container je pokazivac na current html element
        var $container = $("#divZaTabelu");
        $container.empty();

        if (status == "success") {
            console.log(data);
            //ispis naslova
            var div = $("<div></div>");
            var h1 = $("<h1>Festivali</h1>");
            div.append(h1);
            
            //ispis tabele
            var table = $("<table class='table' border=2></table>");
            var tableHeader = $("<tr  style='color:black; background-color:silver'><th><b>Naziv</b></th><th><b>Mesto</b></th><th><b>Godina</b></th><th><b>Cena</b></th><th id='headerObrisi'style='display:none'><b>Obrisi</b></th><th id='headerIzmeni' style='display:none'><b>Izmeni</b></th></tr>");//<th id='headerIzmeni' style='display:block'><b>Izmeni</b></th>
            table.append(tableHeader);
            for (i = 0; i < data.length; i++) {
                //prikazujemo novi red u tabeli
                var row = "<tr>";
                //prikaz podataka
                var displayData = "<td>" + data[i].FestivalName + "</td><td>" + data[i].Place.PlaceName + "</td><td>" + data[i].FirstHeldYear + "</td><td>" + data[i].Price + "</td>";
                var stringId = data[i].Id.toString();
                var displayDelete = "<td  style='display:none'><button id='btnDelete' name=" + stringId + " >Delete</button></td>";//<a href="#nekidrugiIdElementNaSPA" id='btnDelete' title=" + stringId + " >Delete</a>
                var displayEdit = "<td  style='display:none'><button id=btnEdit name=" + stringId + " >Edit</button></td>";
                row += displayData + displayDelete + displayEdit + "</tr>";//+ displayEdit
                table.append(row);

            }
            div.append(table);

            //ispis novog sadrzaja
            $container.append(div);
            if (token) {
                //prikaz funkcije za brisanje i izmenu entiteta
                $("#headerObrisi").css("display", "block");
                $("#headerIzmeni").css("display", "block");
                $("td[style='display:none']").css("display", "block");
            };

        }
        else {
            div = $("<div></div>");
            h1 = $("<h1>Greska prilikom preuzimanja entiteta.</h1>");
            div.append(h1);
            $container.append(div);
        }

    };

    //dodavanje novog entiteta ili izmena entiteta
    $("#formCreate").submit(function (e) {
        //sprecavanje defaultne akcije forme
        e.preventDefault();

        var festivalName = $("#proName").val();
        var festivalPrice = $("#proPrice").val();
        var festivalDate = $("#proDate").val();
        var mestoId = $("#dropBox").val();

        var httpAction;
        var sendData;
        var url = "http://" + host + festivaliEndpoint;
        if (token) {
            httpHeaders.Authorization = 'Bearer ' + token;
        }

        //u zavisnosti od akcije pripremamo objekat

        if (formAction == "Create") {
            httpAction = "POST";

            sendData = {
                "FestivalName": festivalName,
                "Price": festivalPrice,
                "FirstHeldYear": festivalDate,
                "PlaceId": mestoId

            };
        }
        else {
            httpAction = "PUT";
            url = url + editingId.toString();
            sendData = {
                "Id": editingId,
                "FestivalName": festivalName,
                "Price": festivalPrice,
                "FirstHeldYear": festivalDate,
                "PlaceId": mestoId
            };
        };
        console.log(httpHeaders);
        console.log(url);
        console.log("Objekat za slanje");
        console.log(sendData);

        $.ajax({
            url: url,
            type: httpAction,
            headers: httpHeaders,
            data: sendData

        })
            .done(function (data, status) {
                formAction = "Create";
                refreshTable();
                refreshForm();
            })
            .fail(function (data, status) {
                alert("Desila se greska pri POST/PUT api/products/1.OVO JE GRESKA.");
            });


    });

    //Pozivanje brisanja entiteta
    $("#btnDelete").on('click', function () {
        deleteFestival();
    });

    //brisanje entiteta
    function deleteFestival() {
        //izvlacimo {id}
        var deleteId = this.name;
        var urlRequest = "http://" + host + festivaliEndpoint + deleteId.toString();
        console.log("URL zahteva: " + urlRequest);
        if (token) {
            httpHeaders.Authorization = 'Bearer ' + token;
        };

        //saljemo zahtev
        $.ajax({
            url: urlRequest,
            type: "DELETE",
            headers: httpHeaders

        })
            .done(function (data, status) {
                refreshTable();
            }).fail(function (data, status) {
                alert("Desila se greska pri DELETE api/products/1.");
            });
    };


    //izmena entiteta
    function editFestival() {
        //izvlacimo id
        var editId = this.name;
        var urlRequest = "http://" + host + festivaliEndpoint + editId.toString();
        console.log("URL zahteva: " + urlRequest);
        if (token) {
            httpHeaders.Authorization = 'Bearer' + token;
        };
        //saljemo zahtev da dobavimo taj entitet
        $.ajax({
            url: urlRequest,
            type: "GET"

        })
            //popunjavanje forme dobavljenim podacima radi izmene
            .done(function (data, status) {
                $("#proName").val(data.FestivalName);
                $("#proPrice").val(data.Price);
                $("#proDate").val(data.FirstHeldYear);
                $('#dropBox').val(data.PlaceId.toString()).prop('selected', true);//setujemo opciju dropBoxa
                editingId = data.Id;
                formAction = "Update";
            })
            .fail(function (data, status) {
                formAction = "Create";
                alert("Desila se greska prilikom GET api/products/1");
            });

    };

    //metoda za populate dropbox
    function populateDropbox() {
        //dobavljanje niza entiteta-1 sa GET api/entiteti
        var requestUrl = 'http://' + host + mestaEndpoint;
        console.log("URL zahteva: " + requestUrl);
        if (token) {
            httpHeaders.Authorization = 'Bearer' + token;
        }

        $.ajax({
            type: "GET",
            url: requestUrl,
            headers: httpHeaders


        })
            .done(function (data, status) {
                $("#dropBox").empty();
                for (i = 0; i < data.length; i++) {
                    var id = data[i].Id;
                    var name = data[i].PlaceName;
                    $("#dropBox").append("<option value='" + id + "'>" + name + "</option>");

                }
            })
            .fail(function (data, status) {
                alert("Ucitavanje GET api/entitet-1 nije uspelo.")
            });
    };

    // dugme odustajanje od submitovanja forme
    $("#btnOdustajanje").click(function () {
        refreshForm();
        formAction = "Create";

    });

    // osvezavanje prikaza tabele
    function refreshTable() {

        //osvezavam izgled tabele
        $("#btnTabela").on("click", ucitajEntiteteVise).trigger("click");
        if (token) {
            //prikaz funkcije za brisanje i izmenu entiteta
            $("#headerObrisi").css("display", "block");
            $("#headerIzmeni").css("display", "block");
            $("td[style='display:none']").css("display", "block");
        };
    };

    //osvezavanje formCreate
    function refreshForm() {
        //cistim formu
        $("#proName").val('');
        $("#proPrice").val('');
        $("#proDate").val('');
        //populateDropbox();
    };

    //osvezavanje Prijave
    function refreshPrijava() {
        $("#priEmail").val('');
        $("#priLoz").val('');
    }

    //OSTALE FUNKCIONALNOSTI

    //pretraga festivala POST api/festivali/pretraga
    $("#pretraga").click(function (e) {
        var start = $("#start").val();
        var kraj = $("#kraj").val();
        if (start > 1950 && start < 2018 && kraj > start && kraj < 2018) {
            //posalji POST api/festivali/pretraga
            //var startKraj = start.toString() + "," + kraj.toString();
            var httpAction;

            var url = "http://" + host + festivaliEndpoint + "pretraga";
            if (token) {
                httpHeaders.Authorization = 'Bearer ' + token;
            };
            httpAction = "POST";
            var sendData = {

                "Start": start,
                "Kraj": kraj
            };




            console.log(httpHeaders);
            console.log(url);
            console.log("Objekat za slanje");
            console.log(sendData);

            $.ajax({
                url: url,
                type: httpAction,
                headers: httpHeaders,
                data: sendData

            })
                .done(ucitajTabelu)
                .fail(function (data, status) {
                    alert("Desila se greska pri POST api/festivali/pretraga")
                });
        }
        else {
            alert("Vrednosti start i kraj moraju biti celobrojne vrednosti u opsegu 1951 - 2017.");
            e.preventDefault;
            $("#start").val('');
            $("#kraj").val('');

        };
    });




});