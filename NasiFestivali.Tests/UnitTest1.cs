﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NasiFestivali.Interfaces;
using Moq;
using NasiFestivali.Models;
using NasiFestivali.Controllers;
using System.Web.Http;
using System.Web.Http.Results;

namespace NasiFestivali.Tests
{
    /// <summary>
    /// Summary description for UnitTest1
    /// </summary>
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void GetReturnsProductWithSameId()
        {
            //Arrange
            var mockRepository = new Mock<IFestivalsRepository>();
            mockRepository.Setup(x => x.GetById(42)).Returns(new Festival { Id = 42 });

            var controller = new FestivaliController(mockRepository.Object);

            //Act
            IHttpActionResult actionResult = controller.Get(42);
            var contentResult = actionResult as OkNegotiatedContentResult<Festival>;

            //Assert
            Assert.IsNotNull(contentResult);
            Assert.IsNotNull(contentResult.Content);
            Assert.AreEqual(42, contentResult.Content.Id);

        }

        [TestMethod]
        public void GetReturnsNotFound()
        {
            //Arrange
            var mockRepository = new Mock<IFestivalsRepository>();
            var controller = new FestivaliController(mockRepository.Object);

            //Act
            IHttpActionResult actionResult = controller.Get(10);

            //assert
            Assert.IsInstanceOfType(actionResult, typeof(NotFoundResult));
        }

        [TestMethod]
        public void DeleteReturnsOk()
        {
            //Arrange
            var mockRepository = new Mock<IFestivalsRepository>();
            mockRepository.Setup(x => x.GetById(10)).Returns(new Festival { Id = 10 });
            var controller = new FestivaliController(mockRepository.Object);

            //Act
            IHttpActionResult actionResult = controller.Delete(10);

            //Assert
            Assert.IsInstanceOfType(actionResult, typeof(OkResult));
        }

        [TestMethod]
        public void PostMethodSetsLocationHeader()
        {
            //Arrange
            var mockRepository = new Mock<IFestivalsRepository>();
            var controller = new FestivaliController(mockRepository.Object);

            //Act
            IHttpActionResult actionResult = controller.Post(new Festival { Id = 10, FestivalName = "Festival1" });
            var createdResult = actionResult as CreatedAtRouteNegotiatedContentResult<Festival>;

            //Assert
            Assert.IsNotNull(createdResult);
            Assert.AreEqual("DefaultApi", createdResult.RouteName);
            Assert.AreEqual(10, createdResult.RouteValues["id"]);
        }
    }
}
